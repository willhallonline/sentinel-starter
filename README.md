# Sentinel Starter

Policies for getting started with [Sentinel](https://www.hashicorp.com/sentinel/).

## Documentation

Find our more about Sentinel in the [documentation](https://docs.hashicorp.com/sentinel).

## Maintainers

* Will Hall
